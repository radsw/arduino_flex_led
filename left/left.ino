io
#include <RGBmatrixPanel.h>
#include <RGBmatrixPanel1.h>

// Most of the signal pins are configurable, but the CLK pin has some
// special constraints.  On 8-bit AVR boards it must be on PORTB...
// Pin 8 works on the Arduino Uno & compatibles (e.g. Adafruit Metro),
// Pin 11 works on the Arduino Mega.  On 32-bit SAMD boards it must be
// on the same PORT as the RGB data pins (D2-D7)...
// Pin 8 works on the Adafruit Metro M0 or Arduino Zero,
// Pin A4 works on the Adafruit Metro M4 (if using the Adafruit RGB
// matrix1 Shield, cut trace between CLK pads and run a wire to A4).

//#define CLK  8   // USE THIS ON ADAFRUIT METRO M0, etc.
//#define CLK A4 // USE THIS ON METRO M4 (not M0)
#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE   9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2
#define D   A3

#define r_pin 4
#define b_pin 5
#define g_pin 6

//#define mac_pin 7

// master slave connection

#define CLK_1 12 // USE THIS ON ARDUINO MEGA
#define OE_1   8
#define LAT_1 14
#define AA1   A4
#define BB1   A5
#define CC1   A6
#define DD1   A7

int string_length=11;

bool master = true;
bool slave1 = false;

bool incoming = false;
bool outgoing = false;

int input_pin =2;
int output_pin =7;

char current_color='R';
char neon_flash[]=  "RGBYPLOGBLRPY";
bool neon_f= true;
unsigned long time_5=0; 
unsigned long timer=2000;
int rotation=0;

String versn = "Flex 1.1";
bool scroll_on=true;
bool scroll_change=true;
int scrol_length= 64;
int scrol_number=0;

int incomingByte = 0;


bool new_string =true;
bool am_scrolling=false;

RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false, 64);
RGBmatrixPanel1 matrix1(AA1, BB1, CC1, DD1, CLK_1, LAT_1, OE_1, false, 64);

int textsize_display1=1;
String stestring="robotic assistance devices";

int xc1=0; // spacing x axis 
int yc1 =0; // spacing y axis
int xc2=8; // spacing x axis 
int yc2 =8; // spacing y axis
int xc3=16; // spacing x axis 
int yc3 =16; // spacing y axis
int xc4=24; // spacing x axis 
int yc4 =24; // spacing y axis

int number_of_rows=0;
char row_string_1[]=  "Roameo     Roameo     ";
char row_string_2[]=  " Roameo     Roameo     ";
char row_string_3[]= " Roameo     Roameo     ";
char row_string_4[]=  " Roameo     Roameo     ";
char row_string_1_color[]=  "RRRRRRRRRRRRRRRRRRRRRR";
char row_string_2_color[]=  "RRRRRRRRRRRRRRRRRRRRRR";
char row_string_3_color[]= "RRRRRRRRRRRRRRRRRRRRRR";
char row_string_4_color[]= "RRRRRRRRRRRRRRRRRRRRRR";


char row_string_scroll[]=  " Roameo - Rugged Observation Assistance Mobile Electronic Officer ";
char row_string_scroll_color[]= "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR";

//RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
//GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
//BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB

char row_string_color_temp[] = "WWWWWWWWWW";
char row_string_temp1[] = "ABCDEABCDE";

uint16_t Wheel1(char WheelPos);
uint16_t Wheel(char WheelPos);



void read_serial(){
  bool looop = true;
  bool flexLED = false;
   int incomingByte = 0; 
   char x;
   char serialA[]="";
   String incompingString="";
   int increament =0;
   
do
{
   incompingString="";   
   while (Serial.available() > 0) {
                // read the incoming byte:
                incomingByte = Serial.read();
                //digitalWrite(output_pin, LOW);

                x =incomingByte;
               // Serial.print(x);
                

                increament++;
                incompingString = incompingString+x;   
                delay(10);
                string_length=(incompingString.length()-7)/2;
                
                
                if(x== '\n' ){
                  break;
                  }
              }
              // This is for Size 1
              if(incompingString[0]=='5' &&incompingString[1]=='5' ){
                flexLED=true;
                scroll_on=false;
                number_of_rows++;
                //Serial.print(String(number_of_rows) + "  ");
                //Serial.println(incompingString);
                
               if(incompingString[2]=='1'){
                textsize_display1=1;
                if(number_of_rows==1){
                  
                    for(int change_clear=0;change_clear<22;change_clear++ ){
                              row_string_1[change_clear]=  ' ';
                              row_string_1_color[change_clear]=  'W';
                        }
                    

                    xc1= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                    yc1= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc1=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc1=starting_pixel;

                      }                    
                    
                  for(int charnum1=0;charnum1<string_length;charnum1++){
                    row_string_1[charnum1]=incompingString[charnum1+7];
                    row_string_1_color[charnum1]=incompingString[string_length+7];
                    }
                  }
                if(number_of_rows==2){
                    for(int change_clear=0;change_clear<22;change_clear++ ){
                              row_string_2[change_clear]=  ' ';
                              row_string_2_color[change_clear]=  'W';
                        }

                    xc2= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                    yc2= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc2=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc2=starting_pixel;

                      }                    

                   
                  for(int charnum2=0;charnum2<string_length;charnum2++){
                    row_string_2[charnum2]=incompingString[charnum2+7];
                    row_string_2_color[charnum2]=incompingString[string_length+7];
                    }
                  }
                if(number_of_rows==3){
                    for(int change_clear=0;change_clear<22;change_clear++ ){
                              row_string_3[change_clear]=  ' ';
                              row_string_3_color[change_clear]=  'W';
                        }
                    xc3= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                    yc3= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc3=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc3=starting_pixel;

                      }                    
                  for(int charnum=0;charnum<string_length;charnum++){
                    row_string_3[charnum]=incompingString[charnum+7];
                    row_string_3_color[charnum]=incompingString[string_length+7];
                    }
                  }
                if(number_of_rows==4){
                    for(int change_clear=0;change_clear<22;change_clear++ ){
                              row_string_4[change_clear]=  ' ';
                              row_string_4_color[change_clear]=  'W';
                        }
                    xc4= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                    yc4= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc4=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 11-x_char_num;
                    int starting_pixel= x_char_start*6;
                    xc4=starting_pixel;
                      }                    

                  for(int charnum=0;charnum<string_length;charnum++){
                    row_string_4[charnum]=incompingString[charnum+7];
                    row_string_4_color[charnum]=incompingString[string_length+7];
                    }
                  }
                
                // Size two without scrolling                
                } else if (incompingString[2]=='2'){
                    textsize_display1=2;
                    if(number_of_rows==1){
                        xc1= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                        yc1= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 5-x_char_num;
                    int starting_pixel= x_char_start*12;
                    xc1=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 5-x_char_num;
                    int starting_pixel= x_char_start*12;
                    xc1=starting_pixel;
                      }                    

                      for(int charnum1=0;charnum1<string_length;charnum1++){
                        row_string_1[charnum1]=incompingString[charnum1+7];
                        row_string_1_color[charnum1]=incompingString[string_length+7];
                        }
                      }
                    if(number_of_rows==2){
                        xc2= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                        yc2= (incompingString[5]-'0')*10+(incompingString[6]-'0');

                  if(string_length%2==0){
                    int x_char_num = string_length/2;
                    int x_char_start = 5-x_char_num;
                    int starting_pixel= x_char_start*12;
                    xc2=starting_pixel;
                    }else {
                    int x_char_num = (1+string_length)/2;
                    int x_char_start = 5-x_char_num;
                    int starting_pixel= x_char_start*12;
                    xc2=starting_pixel;
                      }                    

                      for(int charnum2=0;charnum2<string_length;charnum2++){
                        row_string_2[charnum2]=incompingString[charnum2+7];
                        row_string_2_color[charnum2]=incompingString[string_length+7];
                        }
                      }                                                   
                  }
                                
                }

              // Neon LEDs
              if(incompingString[0]=='6' &&incompingString[1]=='6' ){
                current_color= incompingString[2];
                neon_change();
                if(incompingString[3]=='1'){

                  neon_f=true;
                  neon_flash[0]= incompingString[4] ;
                  neon_flash[1]= incompingString[5] ;
                  neon_flash[2]= incompingString[6] ;
                  neon_flash[3]= incompingString[7] ;
                  neon_flash[4]= incompingString[8] ;
                  neon_flash[5]= incompingString[9] ;
                  neon_flash[6]= incompingString[10] ;
                  neon_flash[7]= incompingString[11] ;
                  neon_flash[8]= incompingString[12] ;
                  neon_flash[9]= incompingString[13] ;
                  neon_flash[10]= incompingString[14] ;
                  neon_flash[11]= incompingString[15] ;
                  int a = (incompingString[16]-'0');
                  int b = (incompingString[17]-'0'); 
                  int c = (incompingString[18]-'0');
                  int d = (incompingString[19]-'0');
                  rotation=0;
                  timer = a*1000+b*100+c*10+d;
                  //Serial.println(timer);
                  time_5= millis();
                  } else if (incompingString[3]=='0'){
                  neon_f=false;
                    }
            
                

                }

              // Size two with scrolling
              if(incompingString[0]=='8' &&incompingString[1]=='8' ){
                    for(int change_clear=0;change_clear<66;change_clear++ ){
                        row_string_scroll[change_clear]=  ' ';
                        row_string_scroll_color[change_clear]=  'W';
                      }                
                new_string=true;
                scroll_on=true;
                scroll_change=true;
                int inputlenth = incompingString.length();
                if(inputlenth%2==0){
                  inputlenth= inputlenth+1;
                  }
                scrol_length=(inputlenth-7)/2-1;
                if(incompingString[2]=='2'){
                    xc1= (incompingString[3]-'0')*10+(incompingString[4]-'0');
                    yc1= (incompingString[5]-'0')*10+(incompingString[6]-'0');
                  for(int charnum1=0;charnum1<scrol_length;charnum1++){
                    row_string_scroll[charnum1]=incompingString[charnum1+7];
                    row_string_scroll_color[charnum1]=incompingString[charnum1+scrol_length+7];
                    }
                  }                
                }


               delay(100); 
               //Serial.println(row_string_scroll_color);
               //Serial.println(row_string_scroll);
               
               if(incompingString[0]=='E'&&incompingString[1]=='N'&&incompingString[2]=='D'){
                  looop = false; 
                  }
                                 
                             
  }while (looop);
        if(flexLED){
        change_display(); 
        flexLED=false;
        }

}


void neon_change()
{
                  if(current_color=='W'){
                    digitalWrite(r_pin, HIGH);
                    digitalWrite(g_pin, HIGH);
                    digitalWrite(b_pin, HIGH);
                  }         
                if(current_color=='R'){
                    digitalWrite(r_pin, HIGH);
                    digitalWrite(g_pin, LOW);
                    digitalWrite(b_pin, LOW);
                  }         
                if(current_color=='G'){
                    digitalWrite(r_pin, LOW);
                    digitalWrite(g_pin, HIGH);
                    digitalWrite(b_pin, LOW);
                  }         
                if(current_color=='B'){
                    digitalWrite(r_pin, LOW);
                    digitalWrite(g_pin, LOW);
                    digitalWrite(b_pin, HIGH);
                  }
                if(current_color=='O'){
                    digitalWrite(r_pin, LOW);
                    digitalWrite(g_pin, LOW);
                    digitalWrite(b_pin, LOW);
                  }
                if(current_color=='Y'){
                    digitalWrite(r_pin, HIGH);
                    digitalWrite(g_pin, HIGH);
                    digitalWrite(b_pin, LOW);
                  }
                if(current_color=='P'){
                    digitalWrite(r_pin, HIGH);
                    digitalWrite(g_pin, LOW);
                    digitalWrite(b_pin, HIGH);
                  }
                if(current_color=='L'){
                    digitalWrite(r_pin, LOW);
                    digitalWrite(g_pin, HIGH);
                    digitalWrite(b_pin, HIGH);
                  }
                         
  }

void change_display(){
      matrix1.writeFillRect(0,0,64,64,0-0-0);
      matrix1.setTextSize(textsize_display1);     // size 1 == 8 pixels high

      matrix.writeFillRect(0,0,64,64,0-0-0);
      matrix.setTextSize(textsize_display1);     // size 1 == 8 pixels high
      
     int charachtor_len=11;
     int correct_len =11;
     int len =11;
     
      if(number_of_rows==1){
        if(textsize_display1==1){
        charachtor_len=( 66- xc1)/6;
        }else if(textsize_display1==2){
        charachtor_len=( 66- xc1)/12;
          }
        matrix1.setCursor(3, yc1);
        matrix.setCursor(xc1, yc1);

       //   Serial.println("Printing 1" + String(row_string_1));
          for(int colors1=0;colors1< charachtor_len;colors1++){
              matrix.setTextColor(Wheel(row_string_1_color[colors1]));
              matrix.print(row_string_1[colors1]);                        
              matrix1.setTextColor(Wheel1(row_string_1_color[colors1]));
              matrix1.print(row_string_1[colors1+charachtor_len]);                        
          }
      
      } else if(number_of_rows==2){
        if(textsize_display1==1){
        charachtor_len=( 66- xc1)/6;
        }else if(textsize_display1==2){
        charachtor_len=( 66- xc1)/12;
          }
          matrix1.setCursor(3, yc1);
          matrix.setCursor(xc1, yc1);
          for(int colors1=0;colors1< charachtor_len;colors1++){
              matrix1.setTextColor(Wheel1(row_string_1_color[colors1]));
              matrix1.print(row_string_1[colors1+charachtor_len]);   
             // Serial.print(row_string_1[colors1+charachtor_len]);                     
              matrix.setTextColor(Wheel(row_string_1_color[colors1]));
              matrix.print(row_string_1[colors1]);                        
            }

   
        if(textsize_display1==1){
        charachtor_len=( 66- xc2)/6;
        }else if(textsize_display1==2){
        charachtor_len=( 66- xc2)/12;
          }
          matrix1.setCursor(3, yc2);
          matrix.setCursor(xc2, yc2);
          for(int colors2=0;colors2<charachtor_len;colors2++){
              matrix1.setTextColor(Wheel1(row_string_2_color[colors2]));
              matrix1.print(row_string_2[colors2+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_2_color[colors2]));
              matrix.print(row_string_2[colors2]);                        
            }
      
      } else if(number_of_rows==3){
        charachtor_len=( 66- xc1)/6;
          matrix1.setCursor(3, yc1);
          matrix.setCursor(xc1, yc1);
          for(int colors1=0;colors1< charachtor_len;colors1++){
              matrix1.setTextColor(Wheel1(row_string_1_color[colors1]));
              matrix1.print(row_string_1[colors1+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_1_color[colors1]));
              matrix.print(row_string_1[colors1]);                        
            }
        charachtor_len=( 66- xc2)/6;          
          matrix1.setCursor(3, yc2);
          matrix.setCursor(xc2, yc2);
          for(int colors2=0;colors2< charachtor_len;colors2++){
              matrix1.setTextColor(Wheel1(row_string_2_color[colors2]));
              matrix1.print(row_string_2[colors2+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_2_color[colors2]));
              matrix.print(row_string_2[colors2]);                        
            }
        charachtor_len=( 66- xc3)/6;          
          matrix1.setCursor(3, yc3);
          matrix.setCursor(xc3, yc3);
          for(int colors3=0;colors3< charachtor_len;colors3++){
              matrix1.setTextColor(Wheel1(row_string_3_color[colors3]));
              matrix1.print(row_string_3[colors3+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_3_color[colors3]));
              matrix.print(row_string_3[colors3]);                        
            }

      
      } else if(number_of_rows==4){
        charachtor_len=( 66- xc1)/6;
          matrix1.setCursor(1, yc1 - 1);
          matrix.setCursor(xc1 - 1, yc1 - 1);
          for(int colors1=0;colors1< charachtor_len;colors1++){
              matrix1.setTextColor(Wheel1(row_string_1_color[colors1]));
              matrix1.print(row_string_1[colors1+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_1_color[colors1]));
              matrix.print(row_string_1[colors1]);                        
            }
          
        charachtor_len=( 66- xc2)/6;
          matrix1.setCursor(1, yc2 - 1);
          matrix.setCursor(xc2 - 1, yc2 - 1);
          for(int colors2=0;colors2< charachtor_len;colors2++){
              matrix1.setTextColor(Wheel1(row_string_2_color[colors2]));
              matrix1.print(row_string_2[colors2+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_2_color[colors2]));
              matrix.print(row_string_2[colors2]);                        
            }
        charachtor_len=( 66- xc3)/6;
          matrix1.setCursor(1, yc3 - 2);
          matrix.setCursor(xc3 - 1, yc3 - 2);
          for(int colors3=0;colors3< charachtor_len;colors3++){
              matrix1.setTextColor(Wheel1(row_string_3_color[colors3]));
              matrix1.print(row_string_3[colors3+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_3_color[colors3]));
              matrix.print(row_string_3[colors3]);                        
            }

        charachtor_len=( 66- xc4)/6;
          matrix1.setCursor(1, yc4 - 2);
          matrix.setCursor(xc4 - 1, yc4 - 2);
          for(int colors4=0;colors4< charachtor_len;colors4++){
              matrix1.setTextColor(Wheel1(row_string_4_color[colors4]));
              matrix1.print(row_string_4[colors4+charachtor_len]);                        
              matrix.setTextColor(Wheel(row_string_4_color[colors4]));
              matrix.print(row_string_4[colors4]);                        
            }
      
      } else if(number_of_rows==0){
          matrix1.setCursor(xc1, yc1);
          matrix1.println(String(row_string_1));
          matrix1.setCursor(xc2, yc2);
          matrix1.println(String(row_string_2));
          matrix1.setCursor(xc3, yc3);
          matrix1.println(String(row_string_3));
          matrix1.setCursor(xc4, yc4);
          matrix1.println(String(row_string_4));

          matrix.setCursor(xc1, yc1);
          matrix.println(String(row_string_1));
          matrix.setCursor(xc2, yc2);
          matrix.println(String(row_string_2));
          matrix.setCursor(xc3, yc3);
          matrix.println(String(row_string_3));
          matrix.setCursor(xc4, yc4);
          matrix.println(String(row_string_4));
        
        }

 number_of_rows=0;
  }


uint16_t Wheel1(char WheelPos) {
  if(WheelPos == 'W') {
   return matrix1.Color333(7, 7, 7);
  } else if(WheelPos =='R') {
   return matrix1.Color333(7, 0, 0);
  } else if(WheelPos =='Y') {
   return matrix1.Color333(7, 4, 0);
  } else if(WheelPos =='X') {
   return matrix1.Color333(0, 4, 7);
  } else if(WheelPos =='P') {
   return matrix1.Color333(7, 0, 4);
  } else if(WheelPos =='G') {
   return matrix1.Color333(0, 0, 7);
  } else if(WheelPos =='B') {
   return matrix1.Color333(0, 7, 0);
  } else{
   return matrix1.Color333(7, 7, 7);
  } 
}


uint16_t Wheel(char WheelPos) {
  if(WheelPos == 'W') {
   return matrix.Color333(7, 7, 7);
  } else if(WheelPos =='R') {
   return matrix.Color333(7, 0, 0);
  } else if(WheelPos =='Y') {
   return matrix.Color333(7, 4, 0);
  } else if(WheelPos =='X') {
   return matrix.Color333(0, 4, 7);
  } else if(WheelPos =='P') {
   return matrix.Color333(7, 0, 4);
  } else if(WheelPos =='G') {
   return matrix.Color333(0, 0, 7);
  } else if(WheelPos =='B') {
   return matrix.Color333(0, 7, 0);
  } else{
   return matrix.Color333(7, 7, 7);
  } 
}


void scrol_function(){
  
    String changed="";
    if(scroll_on){
    int x=2;
    int y=0;
    
      if(scroll_change){
        scroll_change=false;
        matrix1.writeFillRect(0,0,64,64,0-0-0);
        matrix.writeFillRect(0,0,64,64,0-0-0);
        
        }
        
        int scrol_temp= scrol_number;

        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];
        row_string_temp1[5]= row_string_scroll[scrol_temp+5];
        row_string_temp1[6]= row_string_scroll[scrol_temp+6];
        row_string_temp1[7]= row_string_scroll[scrol_temp+7];
        row_string_temp1[8]= row_string_scroll[scrol_temp+8];
        row_string_temp1[9]= row_string_scroll[scrol_temp+9];

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[scrol_temp+4];
        

        if(scrol_length - scrol_number==9){

      //  am_scrolling = false;
        
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];        
        row_string_temp1[5]= row_string_scroll[scrol_temp+5];
        row_string_temp1[6]= row_string_scroll[scrol_temp+6];
        row_string_temp1[7]= row_string_scroll[scrol_temp+7];
        row_string_temp1[8]= row_string_scroll[scrol_temp+8];
        row_string_temp1[9]= row_string_scroll[0];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[0];
          
          }




        if(scrol_length - scrol_number==8){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];        
        row_string_temp1[5]= row_string_scroll[scrol_temp+5];
        row_string_temp1[6]= row_string_scroll[scrol_temp+6];
        row_string_temp1[7]= row_string_scroll[scrol_temp+7];
        row_string_temp1[8]= row_string_scroll[0];
        row_string_temp1[9]= row_string_scroll[1];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[0];
          
          }


        if(scrol_length - scrol_number==7){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];        
        row_string_temp1[5]= row_string_scroll[scrol_temp+5];
        row_string_temp1[6]= row_string_scroll[scrol_temp+6];
        row_string_temp1[7]= row_string_scroll[0];
        row_string_temp1[8]= row_string_scroll[1];
        row_string_temp1[9]= row_string_scroll[2];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[0];
          
          }


        if(scrol_length - scrol_number==6){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];     
        row_string_temp1[5]= row_string_scroll[scrol_temp+5];
        row_string_temp1[6]= row_string_scroll[0];
        row_string_temp1[7]= row_string_scroll[1];
        row_string_temp1[8]= row_string_scroll[2];
        row_string_temp1[9]= row_string_scroll[3];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[scrol_temp+4];
        row_string_color_temp[5]= row_string_scroll_color[scrol_temp+5];
        row_string_color_temp[6]= row_string_scroll_color[0];
        row_string_color_temp[7]= row_string_scroll_color[1];
        row_string_color_temp[8]= row_string_scroll_color[2];
        row_string_color_temp[9]= row_string_scroll_color[3];
          
          }


        if(scrol_length - scrol_number==5){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[scrol_temp+4];       
        row_string_temp1[5]= row_string_scroll[0];
        row_string_temp1[6]= row_string_scroll[1];
        row_string_temp1[7]= row_string_scroll[2];
        row_string_temp1[8]= row_string_scroll[3];
        row_string_temp1[9]= row_string_scroll[4];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[scrol_temp+4];
        row_string_color_temp[5]= row_string_scroll_color[0];
        row_string_color_temp[6]= row_string_scroll_color[1];
        row_string_color_temp[7]= row_string_scroll_color[2];
        row_string_color_temp[8]= row_string_scroll_color[3];
        row_string_color_temp[9]= row_string_scroll_color[4];
          
          }


        if(scrol_length - scrol_number==4){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[scrol_temp+3];
        row_string_temp1[4]= row_string_scroll[0];        
        row_string_temp1[5]= row_string_scroll[1];
        row_string_temp1[6]= row_string_scroll[2];
        row_string_temp1[7]= row_string_scroll[3];
        row_string_temp1[8]= row_string_scroll[4];
        row_string_temp1[9]= row_string_scroll[5];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[scrol_temp+3];
        row_string_color_temp[4]= row_string_scroll_color[0];
        row_string_color_temp[5]= row_string_scroll_color[1];
        row_string_color_temp[6]= row_string_scroll_color[2];
        row_string_color_temp[7]= row_string_scroll_color[3];
        row_string_color_temp[8]= row_string_scroll_color[4];
        row_string_color_temp[9]= row_string_scroll_color[5];
          
          }

        if(scrol_length - scrol_number==3){
         // am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[scrol_temp+2];
        row_string_temp1[3]= row_string_scroll[0];
        row_string_temp1[4]= row_string_scroll[1];  
        row_string_temp1[5]= row_string_scroll[2];
        row_string_temp1[6]= row_string_scroll[3];
        row_string_temp1[7]= row_string_scroll[4];
        row_string_temp1[8]= row_string_scroll[5];
        row_string_temp1[9]= row_string_scroll[6];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[scrol_temp+2];
        row_string_color_temp[3]= row_string_scroll_color[0];
        row_string_color_temp[4]= row_string_scroll_color[1];
        row_string_color_temp[5]= row_string_scroll_color[2];
        row_string_color_temp[6]= row_string_scroll_color[3];
        row_string_color_temp[7]= row_string_scroll_color[4];
        row_string_color_temp[8]= row_string_scroll_color[5];
        row_string_color_temp[9]= row_string_scroll_color[6];
          
                
          }


        if(scrol_length - scrol_number==2){
          //am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[scrol_temp+1];
        row_string_temp1[2]= row_string_scroll[0];
        row_string_temp1[3]= row_string_scroll[1];
        row_string_temp1[4]= row_string_scroll[2];          
        row_string_temp1[5]= row_string_scroll[3];
        row_string_temp1[6]= row_string_scroll[4];
        row_string_temp1[7]= row_string_scroll[5];
        row_string_temp1[8]= row_string_scroll[6];
        row_string_temp1[9]= row_string_scroll[7];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[scrol_temp+1];
        row_string_color_temp[2]= row_string_scroll_color[0];
        row_string_color_temp[3]= row_string_scroll_color[1];
        row_string_color_temp[4]= row_string_scroll_color[2];
        row_string_color_temp[5]= row_string_scroll_color[3];
        row_string_color_temp[6]= row_string_scroll_color[4];
        row_string_color_temp[7]= row_string_scroll_color[5];
        row_string_color_temp[8]= row_string_scroll_color[6];
        row_string_color_temp[9]= row_string_scroll_color[7];
          

          }

        if(scrol_length - scrol_number==1){
          //am_scrolling = false;
        row_string_temp1[0]= row_string_scroll[scrol_temp];
        row_string_temp1[1]= row_string_scroll[0];
        row_string_temp1[2]= row_string_scroll[1];
        row_string_temp1[3]= row_string_scroll[2];
        row_string_temp1[4]= row_string_scroll[3];   
        row_string_temp1[5]= row_string_scroll[4];
        row_string_temp1[6]= row_string_scroll[5];
        row_string_temp1[7]= row_string_scroll[6];
        row_string_temp1[8]= row_string_scroll[7];
        row_string_temp1[9]= row_string_scroll[8];   

        row_string_color_temp[0]= row_string_scroll_color[scrol_temp];
        row_string_color_temp[1]= row_string_scroll_color[0];
        row_string_color_temp[2]= row_string_scroll_color[1];
        row_string_color_temp[3]= row_string_scroll_color[2];
        row_string_color_temp[4]= row_string_scroll_color[3];
        row_string_color_temp[5]= row_string_scroll_color[4];
        row_string_color_temp[6]= row_string_scroll_color[5];
        row_string_color_temp[7]= row_string_scroll_color[6];
        row_string_color_temp[8]= row_string_scroll_color[7];
        row_string_color_temp[9]= row_string_scroll_color[8];     
          }

        if(scrol_length - scrol_number>8){
            outgoing=true ;
          }
       // int x =0;
       // int y=0;
          delay(100);
          //digitalWrite(10, HIGH);     // Turn off display
          //matrix.writeFillRect(0,0,64,64,0-0-0);
          matrix.setTextSize(2);
          //turnblack();
         
          matrix.drawChar(3,8,row_string_temp1[0],Wheel(row_string_color_temp[0]),0-0-0,2);                                  
          matrix.drawChar(15,8,row_string_temp1[1],Wheel(row_string_color_temp[1]),0-0-0,2);                                  
          matrix.drawChar(27,8,row_string_temp1[2],Wheel(row_string_color_temp[2]),0-0-0,2);                                  
          matrix.drawChar(39,8,row_string_temp1[3],Wheel(row_string_color_temp[3]),0-0-0,2);                                  
          matrix.drawChar(51,8,row_string_temp1[4],Wheel(row_string_color_temp[4]),0-0-0,2);    
                                        
          matrix1.drawChar(0,8,row_string_temp1[5],Wheel1(row_string_color_temp[5]),0-0-0,2);                                  
          matrix1.drawChar(12,8,row_string_temp1[6],Wheel1(row_string_color_temp[6]),0-0-0,2);                                  
          matrix1.drawChar(24,8,row_string_temp1[7],Wheel1(row_string_color_temp[7]),0-0-0,2);                                  
          matrix1.drawChar(36,8,row_string_temp1[8],Wheel1(row_string_color_temp[8]),0-0-0,2);                                  
          matrix1.drawChar(48,8,row_string_temp1[9],Wheel1(row_string_color_temp[9]),0-0-0,2);                                  

          scrol_number++;
          
          if(scrol_number>scrol_length-1){
            scrol_number=0;
            }
            
     

    }

//Serial.println(row_string_temp1);
  
  }

void setup() {

  Serial.begin(9600);

  Serial.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
  Serial.println("LED_firmware " + versn);

  Serial.println("Developed by RADSkunkworks");
  Serial.println("LED Master");

  Serial.println("Red:");
  Serial.print(r_pin);
  
  Serial.println("Green:");
  Serial.print(g_pin);

  Serial.println("Blue:");
  Serial.print(b_pin);

    
  Serial.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
  
  pinMode(r_pin, OUTPUT);
  pinMode(g_pin, OUTPUT);
  pinMode(b_pin, OUTPUT);
  //pinMode(mac_pin, OUTPUT);

  pinMode(output_pin, OUTPUT);
  pinMode(input_pin, INPUT);

  
  matrix1.begin();
  matrix.begin();

  matrix1.fillScreen(matrix1.Color333(0, 0, 0));
  matrix1.setTextWrap(false); // Don't wrap at end of line - will do ourselves
  matrix.fillScreen(matrix.Color333(0, 0, 0));
  matrix.setTextWrap(false); // Don't wrap at end of line - will do ourselves
  change_display();
   digitalWrite(b_pin, HIGH);
}

void loop() {
  if (Serial.available() > 0) {
      read_serial();
    }
      scrol_function();
      if(neon_f){
        if(millis()-time_5>timer){
            current_color=neon_flash[rotation];
            rotation++;
            neon_change();
            if(rotation>11){
              rotation=0;
              }
              if(!scroll_on){
                time_5=millis();}
          
          }
        }    
}
